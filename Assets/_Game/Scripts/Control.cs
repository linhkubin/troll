using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectState { None, Obstacle, Shockwave, Lose, Target }

public class Control : Singleton<Control>
{
    public Transform startPoint;
    public Transform finishPoint;
    public Transform directPoint;
    public Transform handPoint;
    public LineRenderer line;

    //conner
    private List<Point> connerPoints = new List<Point>();
    private List<Vector2> checkPoints = new List<Vector2>();

    //control
    private Vector2 currentPoint;
    private Vector2 lastPoint;

    //cache
    private Camera Camera;
    private ObjectState objectState;
    private RaycastHit2D hit;

    //control
    private bool active = false;
    public SpriteRenderer targetHolder;

    private Vector3 LastConner
    {
        get
        {
            if (connerPoints.Count <= 0)
            {
                return startPoint.position;
            }

            return connerPoints[connerPoints.Count - 1].position;
        }
    }

    private Vector3 PreLastConner
    {
        get
        {
            if (connerPoints.Count <= 1)
            {
                return startPoint.position;
            }

            return connerPoints[connerPoints.Count - 2].position;
        }
    }

    private Vector2 HeadPoint
    {
        get
        {
            return finishPoint.position;
        }
    }

    private void Awake()
    {
        Camera = Camera.main;
    }

    private void Start()
    {
        //test
        OnInit();
    }

    // Update is called once per frame
    void Update()
    {
        if (active && GameManager.Instance.IsState(GameState.GamePlay))
        {

            if (Input.GetMouseButtonDown(0))
            {
                currentPoint = Camera.ScreenToWorldPoint(Input.mousePosition);
                lastPoint = currentPoint;
            }

            if (Input.GetMouseButton(0))
            {
                //move finish point
                ControlPoint();

                //logic
                //move va khoi tao list points -> neu va cham thi check raycast va them diem conner
                MoveCheckDot(LastConner, finishPoint.position);

                //check remove conner
                CheckRemoveConner(finishPoint.position);
                //render
                RenderLine();
            }

            if (Input.GetMouseButtonUp(0))
            {
                //check xem co thu ve dc hay k

                if (CheckOverlapTarget(handPoint.position))
                {
                    Collect();
                }
            }
        }
    }

    public void SetControl(Vector2 startPoint, Vector2 finishPoint, Quaternion handRotation)
    {
        OnInit();
        this.startPoint.position = startPoint;
        this.finishPoint.position = finishPoint;
        directPoint.rotation = handRotation;
    }

    private void OnInit()
    {
        line.positionCount = 0;
        checkPoints.Clear();

        Point checkConner = connerPoints.Count > 0 ? connerPoints[connerPoints.Count - 1] : null;

        while (checkConner != null)
        {
            int lastIndex = connerPoints.Count - 1;
            connerPoints[lastIndex].centerObst.HideOutline();
            connerPoints.RemoveAt(lastIndex);
            checkConner = connerPoints.Count > 0 ? connerPoints[lastIndex - 1] : null;
        }

        targetHolder.gameObject.SetActive(false);
        finishPoint.gameObject.SetActive(true);
        gameObject.SetActive(true);
        active = true;
    }

    public void OutControl()
    {
        active = false;
        gameObject.SetActive(false);
    }

    public void Collect()
    {
        active = false;
        GameManager.Instance.ChangeState(GameState.Pause);

        targetHolder.sprite = LevelManager.Instance.GetTargetSprite();
        targetHolder.gameObject.SetActive(targetHolder.sprite != null);
        //start pull
        LevelManager.Instance.TakeTarget();
        StartCoroutine(IECollect());
    }

    private IEnumerator IECollect()
    {
        while (connerPoints.Count > 0 || Vector2.Distance(startPoint.position, finishPoint.position) > 0.1f)
        {
            finishPoint.position = Vector3.MoveTowards(finishPoint.position, LastConner, Time.deltaTime * 5f);
            CheckRemoveConner(finishPoint.position);
            RenderLine();

            directPoint.up = (finishPoint.position - LastConner).normalized;

            yield return null;
        }


        yield return Cache.GetWFS(0.5f);

        LevelManager.Instance.Victory();

        gameObject.SetActive(false);
    }

    public void ControlPoint()
    {
        currentPoint = (Camera.ScreenToWorldPoint(Input.mousePosition));
        Vector2 target = (currentPoint - lastPoint) + (Vector2)finishPoint.position;

        if (CheckRaycast(finishPoint.position, target) != ObjectState.Obstacle)
        {
            finishPoint.position = target;
        }

        directPoint.up = (finishPoint.position - LastConner).normalized;

        lastPoint = currentPoint;

    }

    public void InitCheckDot(Vector2 start, Vector2 finish)
    {
        checkPoints.Clear();
        Vector2 point = start;

        while (Vector2.Distance(point, finish) > 0.1f)
        {
            point = Vector2.MoveTowards(point, finishPoint.position, .1f);
            checkPoints.Add(point);
        }
    }

    public void MoveCheckDot(Vector2 start, Vector2 finish)
    {
        Vector2 point = start;
        int i = 0;
        bool isRayToObst = false;

        List<Point> points = new List<Point>();

        while (Vector2.Distance(point, finish) > 0.1f)
        {
            point = Vector2.MoveTowards(point, finishPoint.position, .1f);

            if (i < checkPoints.Count)
            {
                ObjectState obj = CheckRaycast(checkPoints[i], point);
                if (obj == ObjectState.Obstacle)
                {
                    ObjectEntity center = null;
                    Vector2 hitPoint = GetHitPoint(checkPoints[i], point, ref center);

                    if (CheckRaycast(hitPoint, start) != ObjectState.Obstacle)
                    {
                        points.Add(new Point(hitPoint, hitPoint - (Vector2)(points.Count <= 0 ? start : points[points.Count - 1].position), center));
                        //points.Add(hitPoint);
                        isRayToObst = true;
                    }

                }
                else if (obj == ObjectState.Lose)
                {
                    ObjectEntity center = null;
                    GetHitPoint(checkPoints[i], point, ref center);
                    center.ShowOutline();
                }
                else
                {
                    checkPoints[i] = point;
                }

            }
            else
            {
                checkPoints.Add(point);
            }

            i++;
        }

        //filter
        //neu co 1 diem nhin thay ca 2 thi add diem do va boa qua het
        //neu co cac diem k nhin thay ca 2 thi lay so dau + so cuoi + them 2 dau
        int startIndex = -1, finishIndex = -1;
        for (int j = 0; j < points.Count; j++)
        {
            if (CheckRaycast(points[j].position, start) != ObjectState.Obstacle)
            {
                if (CheckRaycast(points[j].position, finish) != ObjectState.Obstacle)
                {
                    connerPoints.Add(new Point(points[j].position, points[j].position - (Vector2)LastConner, points[j].centerObst));
                    points[j].centerObst.ShowOutline();
                }
            }
            else
            {
                if (CheckRaycast(points[j].position, finish) == ObjectState.Obstacle && startIndex == -1)
                {
                    startIndex = j - 1;
                }
                else if (finishIndex == -1)
                {
                    finishIndex = j;
                }
            }
        }

        if (startIndex != -1 && finishIndex != -1)
        {
            for (int k = startIndex; k < finishIndex; k++)
            {
                connerPoints.Add(new Point(points[k].position, points[k].position - (Vector2)LastConner, points[k].centerObst));
                points[k].centerObst.ShowOutline();
            }
        }


        if (isRayToObst)
        {
            InitCheckDot(LastConner, finish);
        }
        else
        {
            while (i < checkPoints.Count)
            {
                checkPoints.RemoveAt(checkPoints.Count - 1);
            }
        }

    }

    public void CheckRemoveConner(Vector2 finish)
    {
        Point checkConner = connerPoints.Count > 0 ? connerPoints[connerPoints.Count - 1] : null;

        while (checkConner != null && checkConner.IsUnlock(finish))
        {
            int lastIndex = connerPoints.Count - 1;
            connerPoints[lastIndex].centerObst.HideOutline();
            connerPoints.RemoveAt(lastIndex);
            checkConner = connerPoints.Count > 0 ? connerPoints[lastIndex - 1] : null;
        }
    }

    public void CheckConnerByRaycast(Vector2 start, Vector2 finish)
    {
        //check tu start toi finish, neu co va cham thi add them diem conner point va check new start tiep den khi k con va cham nua
        if (CheckRaycast(start, finish) == ObjectState.Obstacle)
        {
            ObjectEntity center = null;
            Vector2 hitPoint = GetHitPoint(start, start, ref center);

            connerPoints.Add(new Point(hitPoint, hitPoint - (Vector2)LastConner, center));

            CheckConnerByRaycast(LastConner, finish);
        }
    }

    private bool CheckOverlapTarget(Vector2 position)
    {
        return Physics2D.OverlapCircle(position, 0.15f, 64) != null;
    }

    public ObjectState CheckRaycast(Vector2 start, Vector2 finish)
    {
        objectState = ObjectState.None;
        // Cast a ray straight down.
        hit = Physics2D.Raycast(start, finish - start, Vector2.Distance(start, finish));

        // If it hits something...
        if (hit.collider != null)
        {
            //fix late
            //objectState = hit.collider.GetComponent<ObjectEntity>().objectState;
            objectState = Cache.GetObjectEntity(hit.collider).objectState;
        }

        return objectState;
    }

    public Vector2 GetHitPoint(Vector2 start, Vector2 finish, ref ObjectEntity center)
    {
        // Cast a ray straight down.
        hit = Physics2D.Raycast(start, finish - start, Vector2.Distance(start, finish));

        // If it hits something...
        if (hit.collider != null)
        {
            center = Cache.GetObjectEntity(hit.collider);
            return hit.point + (hit.point - (Vector2)center.tf.position).normalized * .1f;
        }

        return Vector3.zero;
    }

    public void RenderLine()
    {
        line.positionCount = connerPoints.Count + 2;
        line.SetPosition(0, (Vector2)startPoint.position);
        for (int i = 1; i < line.positionCount - 1; i++)
        {
            line.SetPosition(i, connerPoints[i - 1].position);
        }
        line.SetPosition(connerPoints.Count + 1, HeadPoint);

    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawSphere(handPoint.position, 0.15f);
    //}
}


[System.Serializable]
public class Point
{
    public Vector2 position;
    public Vector2 directCP;
    public ObjectEntity centerObst;

    public Point(Vector2 position, Vector2 directCP, ObjectEntity centerObst)
    {
        this.position = position;
        this.directCP = directCP;
        this.centerObst = centerObst;
    }

    public bool IsUnlock(Vector2 mousePoint)
    {
        //if ((RelativePosition(directCP, position, centerObst) * RelativePosition(directCP, position, mousePoint)) < 0)
        //{
        //    Debug.Log(RelativePosition(directCP, position, centerObst) + " ___ " + RelativePosition(directCP, position, mousePoint));
        //}
        return (RelativePosition(directCP, position, centerObst.tf.position) * RelativePosition(directCP, position, mousePoint)) < 0 || (Vector2.Distance(position, mousePoint) < 0.1f);
    }

    private float RelativePosition(Vector2 directCP, Vector2 point, Vector2 checkPoint)
    {
        return (checkPoint.x - point.x) / directCP.x - (checkPoint.y - point.y) / directCP.y;
    }
}