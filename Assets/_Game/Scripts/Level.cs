using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public enum State { Start, Win, Lose_1, Lose_2, Lose_3 }

    private State state;

    public GameObject[] gameStateGos;

    public Transform mind;
    public TargetEntity target;
    public Transform startPoint;
    public Transform handPoint;
    public Transform handDirect;

    public bool isTakeTarget = true;

    public void OnInit()
    {
        startPoint.gameObject.SetActive(false);
        handPoint.gameObject.SetActive(false);
        target.SetActive(true);
        Control.Instance.SetControl(startPoint.position, handPoint.position, handDirect.rotation);

        for (int i = 0; i < gameStateGos.Length; i++)
        {
            gameStateGos[i].SetActive(false);
        }
    }

    public void ChangeState(State state, bool direct)
    {
        if (!gameObject.activeInHierarchy)
        {
            gameObject.SetActive(true);
        }
        StartCoroutine(IEChangeState(state, direct));
    }

    public Sprite GetSprite()
    {
        return isTakeTarget ? target.GetSprite() : null;
    }

    private IEnumerator IEChangeState(State state,bool direct)
    {
        if (!direct)
        {
            UI_Game.Instance.OpenUI(UIID.White);

            yield return Cache.GetWFS(0.5f);
        }

        gameStateGos[(int)this.state].SetActive(false);

        this.state = state;

        gameStateGos[(int)this.state].SetActive(true);

        if (state == State.Win)
        {
            target.SetActive(false);
        }
    }
}
