using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetEntity : ObjectEntity
{
    public SpriteRenderer spriteRenderer;


    private void Start()
    {
        outlineGO.GetComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite;
    }


    public Sprite GetSprite()
    {
        return spriteRenderer.sprite;
    }

    public void SetActive(bool active)
    {
        gameObject.SetActive(active);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ShowOutline();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        HideOutline();
    }
}
