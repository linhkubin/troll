using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasVictory : UICanvas
{
    public UserData userData;

    public void RetryButton()
    {
        LevelManager.Instance.OnLoadLevel();
        Close();
    }

    public void NextButton()
    {
        userData.SetIntData(UserData.Key_Level, ref userData.PlayingLevel, userData.PlayingLevel + 1);
        LevelManager.Instance.OnLoadLevel();
        Close();
    }
}
