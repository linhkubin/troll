using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasFail : UICanvas
{
    public void RetryButton()
    {
        LevelManager.Instance.OnLoadLevel();
        Close();
    }

    public void SkipButton()
    {
        LevelManager.Instance.OnLoadLevel();
        Close();
    }
}
