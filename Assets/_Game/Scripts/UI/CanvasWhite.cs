using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasWhite : UICanvas
{
    public override void Open()
    {
        base.Open();

        Invoke("Close", 1f);
    }

}
