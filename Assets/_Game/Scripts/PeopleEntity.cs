using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleEntity : ObjectEntity
{
    [Header("Level State")]
    public Level.State state;

    private void Start()
    {
        outlineGO.GetComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite;
    }

    public override void ShowOutline()
    {
        if (GameManager.Instance.IsState(GameState.GamePlay))
        {
            base.ShowOutline();
            if (state == Level.State.Win)
            {
                Control.Instance.Collect();
                //LevelManager.Instance.Victory();
            }
            else
            {
                LevelManager.Instance.Fail(state);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ShowOutline();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ShowOutline();
    }
}
