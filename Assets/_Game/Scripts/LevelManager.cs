using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelManager : Singleton<LevelManager>
{
    public Level[] levels;
    private Level level;

    public Level testLevel;

    public UserData userData;

    private void Start()
    {
        //TODO: fix late
        userData.PlayingLevel = 7;
        GameManager.Instance.ChangeState(GameState.GamePlay);
        OnLoadLevel();
    }

    public void OnInit()
    {
        level.OnInit();
    }

    public void OnReset()
    {

    }


    public void OnLoadLevel()
    {
        OnReset();
        //xoa cai cu, tao cai moi
        if (level != null)
        {
            Destroy(level.gameObject);
        }

        level = testLevel != null ? Instantiate(testLevel) : Instantiate(levels[userData.PlayingLevel]);

        OnInit();

        ChangeState(Level.State.Start, true);

        GameManager.Instance.ChangeState(GameState.GamePlay);
    }

    public void ChangeState(Level.State state, bool direct)
    {
        level.ChangeState(state, direct);
    }

    public void Victory()
    {
        GameManager.Instance.ChangeState(GameState.Pause);
        ChangeState(Level.State.Win, false);

        StartCoroutine(IEDelayAction(() =>
        {
            UI_Game.Instance.OpenUI(UIID.Victory);
        }, 2.5f));
    }

    public void Fail(Level.State state)
    {
        if (GameManager.Instance.IsState(GameState.GamePlay))
        {
            GameManager.Instance.ChangeState(GameState.Pause);
            StartCoroutine(IEDelayAction(() =>
            {
                Control.Instance.OutControl();
                ChangeState(state, false);
                StartCoroutine(IEDelayAction(() =>
                {
                    UI_Game.Instance.OpenUI(UIID.Fail);
                }, 2f));
            }, 2.5f));
        }
    }

    public Sprite GetTargetSprite()
    {
        return level.GetSprite();
    }

    public void TakeTarget()
    {
        if (level.isTakeTarget)
        {
            level.target.SetActive(false);
        }
    }

    private IEnumerator IEDelayAction(UnityAction action, float time)
    {
        yield return Cache.GetWFS(time);
        action?.Invoke();
    }
}
