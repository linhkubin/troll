using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectEntity : GameUnit
{
    public ObjectState objectState;
    public GameObject outlineGO;
    private int showOutline;

    private void OnEnable()
    {
        showOutline = 0;
        outlineGO.SetActive(false);
    }
    
    public virtual void ShowOutline()
    {
        showOutline++;

        outlineGO.SetActive(showOutline > 0);
    }

    public virtual void HideOutline() 
    {
        showOutline = showOutline <= 0 ? 0 : showOutline -= 1;

        outlineGO.SetActive(showOutline > 0);
    }
}
