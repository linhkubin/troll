using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CounterTime
{
    private UnityAction CounterDoneAction;
    public float m_Time;

    public void CounterStart(UnityAction startAction, UnityAction doneAction, float time)
    {
        startAction?.Invoke();
        CounterDoneAction = doneAction;
        m_Time = time;
    }

    public void CounterExecute()
    {
        if (m_Time > 0)
        {
            m_Time -= Time.deltaTime;

            if (m_Time <= 0)
            {
                CounterExit();
            }
        }
    }

    private void CounterExit()
    {
        CounterDoneAction?.Invoke();
    }
}
