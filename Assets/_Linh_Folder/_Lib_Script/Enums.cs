﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enums
{

}

public enum GameMode { Normal, Challenge }

public enum GameState { MainMenu, Pause, GamePlay }

public enum GameResult { Win, Lose }

public enum UIID{ GamePlay, BlockRaycast, MainMenu, Setting, 
    Victory,
    Fail,
    LoadingGamePlay,
    ChangeScene,
    White,
}

public enum ButtonState { Buy, Equip, Equipped, Ads, CommingSoon }

public enum PriceType { Ads, Gem}

public enum MinigameHard { Hard_1, Hard_2, Hard_3, Hard_4 }

public enum MinigameID {
    PickCoconut,
    MakeFire,
    CutCoconut,
    CraftAxe,
    MainHouse,
    CraftArchery,
    CuttingTree,
    Hunting,
}

[System.Serializable]
public class SkinPrice
{
    public PriceType priceType;
    public int value;
}


