﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant 
{
    public static readonly string TAG_BURGER = "Burger";
    public static readonly string TAG_PLAYER = "Player";

    public static readonly string ANIM_IDLE = "idle";
    public static readonly string ANIM_AIMING = "aiming";
    public static readonly string ANIM_FREE = "free";
    public static readonly string ANIM_POWER_UP = "powerup";
    public static readonly string ANIM_THROW = "throw";
    public static readonly string ANIM_VICTORY = "victory";
    public static readonly string ANIM_PUSH_BACK = "pushBack";
    public static readonly string ANIM_STUN = "stun";
    public static readonly string ANIM_SCARE = "scare";
    public static readonly string ANIM_DANCE = "dance";
    public static readonly string ANIM_ZOMBIEWALK = "zombiewalk";

    public enum Status { Perfect, Great, Cool, Miss }

    public static readonly float FORCE_RATE = 3.5f;

    public static readonly Vector3 DIRECT_RIGHT = new Vector3(0, 0, 0);
    public static readonly Vector3 DIRECT_LEFT = new Vector3(0, 180, 0);

}

