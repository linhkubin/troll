﻿using UnityEngine;
using System.Collections.Generic;


public class Cache
{

    private static Dictionary<float, WaitForSeconds> m_WFS = new Dictionary<float, WaitForSeconds>();

    public static WaitForSeconds GetWFS(float key)
    {
        if(!m_WFS.ContainsKey(key))
        {
            m_WFS[key] = new WaitForSeconds(key);
        }

        return m_WFS[key];
    }

    //------------------------------------------------------------------------------------------------------------


    private static Dictionary<Collider2D, ObjectEntity> m_ObjectEntity = new Dictionary<Collider2D, ObjectEntity>();

    public static ObjectEntity GetObjectEntity(Collider2D key)
    {
        if (!m_ObjectEntity.ContainsKey(key))
        {
            ObjectEntity character = key.GetComponent<ObjectEntity>();

            if (character != null)
            {
                m_ObjectEntity.Add(key, character);
            }
            else
            {
                return null;
            }
        }

        return m_ObjectEntity[key];
    }

    //private static Dictionary<Collider, PlayerLanePoint> m_PlayerLanePoint = new Dictionary<Collider, PlayerLanePoint>();

    //public static PlayerLanePoint GetPlayerLanePoint(Collider key)
    //{
    //    if (!m_PlayerLanePoint.ContainsKey(key))
    //    {
    //        PlayerLanePoint character = key.GetComponent<PlayerLanePoint>();
    //        m_PlayerLanePoint.Add(key, character);
    //    }

    //    return m_PlayerLanePoint[key];
    //}


}
